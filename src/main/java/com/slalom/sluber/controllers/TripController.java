package com.slalom.sluber.controllers;

import com.slalom.sluber.api.SluberApi;
import com.slalom.sluber.api.models.CreateTripDetails;
import com.slalom.sluber.api.models.EmployeeDetails;
import com.slalom.sluber.api.models.TripDetails;
import com.slalom.sluber.services.TripService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
public class TripController implements SluberApi {
    private final TripService tripService;

    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @Override
    public ResponseEntity<TripDetails> createTrip(CreateTripDetails createTripDetails) {
        TripDetails tripDetails = this.tripService.createTrip(createTripDetails);
        return ResponseEntity.ok(tripDetails);
    }

    @Override
    public ResponseEntity<List<TripDetails>> getTrips() {
        return ResponseEntity.ok(this.tripService.getTrips());
    }

    @Override
    public ResponseEntity<TripDetails> addPassenger(String tripId, EmployeeDetails body) {
        return ResponseEntity.ok(this.tripService.addPassenger(tripId, body));
    }
}

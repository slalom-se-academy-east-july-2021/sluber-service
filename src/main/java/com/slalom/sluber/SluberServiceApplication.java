package com.slalom.sluber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SluberServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SluberServiceApplication.class, args);
	}

}

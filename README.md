# README

The SLUBER service is a RESTful service which performs operations on passengers and drivers.

## Installation
This project requires JDK 14

* Install Java JDK - [Link](https://www.oracle.com/java/technologies/javase-jdk16-downloads.html)
	* Download Windows x64 Installer
	* Run Installer
* Installl Maven - [Link](https://maven.apache.org/download.cgi?Preferred=ftp://ftp.osuosl.org/pub/apache/)
	* Download Binary zip archive
	* Extract to safe location
	* Update PATH with location of bin folder in that extract.  **NOTE:  Your PATH must have the \bin folder included in it!**
	
* Install Node - [Link](https://nodejs.org/en/)
* Install IntelliJ Community Edition - [Link](https://www.jetbrains.com/idea/download/?fromIDE=#section=windows)

* Clone repo: https://bitbucket.org/slalom-se-academy-east-july-2021/sluber-service
* Run the following from the command line - `mvn clean install`

## Running in Debug Mode
* From the command line
	* Run the following from the command line - `mvn spring-boot:run` 

* From IntelliJ
	* Open the project in IntelliJ and click the 'Debug' button

* Navigate to http://localhost:8080/swagger-ui

## Building and running the Docker image

* Build Docker image:  docker build -t sluber-service -f ./Dockerfile .

* Run Docker image:  docker run -p 8080:8080 --name sluber-service sluber-service